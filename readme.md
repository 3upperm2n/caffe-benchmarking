automatic profiling scripts, put it at the source level

You need to add gpu test to the file located at
$(CAFFE_ROOT)/include/caffe/test/test_caffe_main.hpp
```cpp
typedef ::testing::Types<GPUDevice<float>, GPUDevice<double> >                  
                         TestDtypesAndGpuDevices; 
```


Use the following bashcript to generate profiling results (csv format) for the caffe tests.

``` bash
#!/bin/bash

if [ ! -d test_mnist ];then
  echo "creating test_mnist folder"
  mkdir test_mnist
fi

cd test_mnist

if [ ! -d caffe-benchmarking ];then
  echo "fetching github code"
  git clone https://3upperm2n@bitbucket.org/3upperm2n/caffe-benchmarking.git
fi

cp caffe-benchmarking/mnist/test_gpu/*.cpp ../src/caffe/test/

# gpu kernels
cd caffe-benchmarking/mnist/test_gpu/


echo "extracting gpu test objects"
gputest_files=`ls | cut -d "." -f1`

# to the source level
cd ../../../../

# make tests
echo "generating test objects"
make -j$(nproc) test

cd ./build/test

# define the number of gpu devices on your current platform
GPUNUM=2

echo "profiling in progress"
for ((dev=0; dev<$GPUNUM; dev++)) 
do
  echo "Running on device : " $dev

  for f in $gputest_files;
  do
	# add extension
	target_obj="$f".testbin
	echo "start profiling " $target_obj

	# output file format : device number + filename + metrics + csv
	nvprof --print-gpu-trace ./$target_obj $dev  2>  dev"$dev"_"$f"_gpuTrace.csv
	nvprof --print-api-summary ./$target_obj $dev  2>  dev"$dev"_"$f"_apiSummary.csv
	nvprof --metrics all ./$target_obj $dev  2>  dev"$dev"_"$f"_metrics.csv
  
	echo "end profiling " $target_obj
  done
done


echo "end of profiling"
echo "check the csv files. Exiting ..."

```
