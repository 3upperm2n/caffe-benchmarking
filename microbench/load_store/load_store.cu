#include <stdio.h>
#include <stdlib.h>                                                             
#include <string.h>                                                             
#include <math.h>                                                               
#include <iostream>
#include <string>


#include <cuda.h>
#include <cuda_runtime.h>
#include <device_functions.h> 

#include "repeat.h"

#define FMIN(x,y) (((x)<(y))?(x):(y)) 
#define FMAX(x,y) (((x)>(y))?(x):(y)) 

void load_test();
void store_test();

__global__ void kernel_load (float *my_array, float a, float b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;
  unsigned int start_time4;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;
  unsigned int end_time4;

  __syncthreads();

  start_time1 = clock();
  end_time1 = clock();

  __syncthreads();

  // 1 load
  start_time2 = clock();
  float d01 = my_array[1];
  end_time2 = clock();

  __syncthreads();

  // 2 load
  start_time3 = clock();
  float d04 = my_array[3];
  float d05 = my_array[4];
  end_time3 = clock();

  __syncthreads();

  start_time4 = clock();
  float d06 = my_array[3];
  float d07 = my_array[4];
  end_time4 = clock();


  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;
  start_t[3] = start_time4;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;
  end_t[3] = end_time4;

  my_array[0] = d01 + d04 + d05 + d06 + d07;
}


__global__ void kernel_store (float *my_array, float a, float b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  float d01 = a + 1.1f;
  float d02 = a + 2.1f;
  float d03 = a + 3.1f;
  float d04 = a + 4.1f;
  float d05 = a + 5.1f;

  __syncthreads();

  // clock overhead
  start_time1 = clock();
  end_time1 = clock();

  __syncthreads();

  // 1 store 
  start_time2 = clock();
  my_array[1] = d01;
  end_time2 = clock();

  __syncthreads();

  // 2 store
  start_time3 = clock();
  my_array[2] = d02;
  my_array[3] = d03;
  end_time3 = clock();

  __syncthreads();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = d01 + d02 + d03 + d04 + d05;
}

int main(int argc, char **argv) {

	int devid=0;
	if(argc == 2)
		devid = atoi(argv[1]);

	cudaSetDevice(devid);

	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, devid);
	printf("Device name: %s\n", prop.name);

	//load_test();
	store_test();

	return 0;

}




void load_test()
{
	float *d_a = NULL;
	cudaMallocManaged(&d_a, sizeof(float) * 64); 

	int clock_num = 8;

	uint *d_start = NULL;
	cudaMallocManaged(&d_start, sizeof(uint) * clock_num); 

	uint *d_end = NULL;
	cudaMallocManaged(&d_end, sizeof(uint) * clock_num); 

	float a = 6;
	float b = 3;

	for(int i=0; i<64; i++)
	{
		d_a[i] = static_cast<float>(i);
	}

	cudaDeviceSynchronize();

	std::string test_name;

	kernel_load <<< 1, 1 >>> (d_a, a, b, d_start, d_end);

	cudaDeviceSynchronize();

	printf("clock overhead :");
	printf("%.3f \n", float(d_end[0] - d_start[0]));

	printf("1 load :\t");
	printf("%.3f \n", float(d_end[1] - d_start[1]));

	printf("2 load :\t");
	printf("%.3f \n", float(d_end[2] - d_start[2]));
	printf("2 load :\t");
	printf("%.3f \n", float(d_end[3] - d_start[3]));


	/*
	for(int i=0; i<clock_num; i++) {
		printf("%.3f \n", float(d_end[i] - d_start[i]));
	}
	*/

	cudaFree(d_a);
	cudaFree(d_start);
	cudaFree(d_end);

	cudaDeviceReset();
}

void store_test()
{
	float *d_a = NULL;
	cudaMallocManaged(&d_a, sizeof(float) * 64); 

	int clock_num = 8;

	uint *d_start = NULL;
	cudaMallocManaged(&d_start, sizeof(uint) * clock_num); 

	uint *d_end = NULL;
	cudaMallocManaged(&d_end, sizeof(uint) * clock_num); 

	float a = 6;
	float b = 3;

	for(int i=0; i<64; i++)
	{
		d_a[i] = static_cast<float>(i);
	}

	cudaDeviceSynchronize();

	std::string test_name;

	kernel_store <<< 1, 1 >>> (d_a, a, b, d_start, d_end);

	cudaDeviceSynchronize();

	printf("clock overhead :");
	printf("%.3f \n", float(d_end[0] - d_start[0]));

	printf("1 store:\t");
	printf("%.3f \n", float(d_end[1] - d_start[1]));

	printf("2 store:\t");
	printf("%.3f \n", float(d_end[2] - d_start[2]));

/*
	for(int i=0; i<clock_num; i++) {
		printf("%.3f \n", float(d_end[i] - d_start[i]));
	}
	*/

	cudaFree(d_a);
	cudaFree(d_start);
	cudaFree(d_end);

	cudaDeviceReset();
}
