#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>


__global__ void shared_latency (float *my_array, 
		float a, 
		float b,  
		uint *start_t, 
		uint *end_t)
{
	unsigned int start_time1;
	unsigned int start_time2;

	unsigned int end_time1;
	unsigned int end_time2;

	float i = a;
	float j = b;

	__syncthreads();

	// clocking overhead
	start_time1 = clock();
	end_time1 = clock();

	__syncthreads();


	start_time2 = clock();
	// mov
	asm("mov.f32 %0, %1;" : "=f"(i): "f"(j));
	end_time2 = clock();

	__syncthreads();


	start_t[0] = start_time1;
	start_t[1] = start_time2;

	end_t[0] = end_time1;
	end_t[1] = end_time2;

	my_array[0] = i + j; 
}



int main(int argc, char **argv) {

	int devid=0;
	if(argc == 2)
		devid = atoi(argv[1]);

	cudaSetDevice(devid);

	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, devid);
	printf("Device name: %s\n", prop.name);

	printf("---------------------------\n"
	" Kepler will have 1 extra mov\n"
	" Maxwell will have 2 extra mov\n"
	" Check the SASS. $make && ./run && vim trace\n"
	"---------------------------\n"
	);

	float *d_a = NULL;
	cudaMallocManaged(&d_a, sizeof(float) * 64); 

	uint *d_start = NULL;
	cudaMallocManaged(&d_start, sizeof(uint) * 3); 

	uint *d_end = NULL;
	cudaMallocManaged(&d_end, sizeof(uint) * 3); 

	for(int i=0; i<64; i++)
	{
		d_a[i] = (float) i;
	}

	cudaDeviceSynchronize();

	size_t sharedMemSize =  sizeof(float) * 64;

	float a = 3.3;
	float b = 1.1;

	shared_latency <<< 1, 1, sharedMemSize>>>(d_a, a, b, d_start, d_end);

	cudaDeviceSynchronize();

	printf("clock overheads: %u clk\n", (d_end[0] - d_start[0]));
	printf("mov instructions: %u clk\n", (d_end[1] - d_start[1]));


	cudaFree(d_a);
	cudaFree(d_start);
	cudaFree(d_end);

	cudaDeviceReset();


	return 0;

}
