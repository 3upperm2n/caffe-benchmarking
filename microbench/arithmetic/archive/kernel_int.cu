/*
   integer type

   ADD
   add.s32     %r5, %r1, %r2; 
   SUB
   sub.s32     %r5, %r2, %r1;
   MAD
   mul.lo.s32  %r5, %r2, %r1; 
   add.s32     %r6, %r5, %r1;
   MUL
   mul.lo.s32  %r5, %r2, %r1; 

   DIV
   div.u32     %r5, %r1, %r2; 
   REM
   rem.u32     %r5, %r1, %r2;
   MIN
   min.u32     %r5, %r1, %r2;
   MAX
   max.u32     %r5, %r1, %r2;
   AND
   and.b32     %r5, %r1, %r2; 
   OR
   or.b32      %r5, %r1, %r2; 
   XOR
   xor.b32     %r5, %r1, %r2;
   SHL
   shl.b32     %r5, %r1, %r2;
   SHR
   shr.u32     %r5, %r1, %r2;
   UMUL24
   mul24.lo.u32    %r5, %r1, %r2;
   UMULHI
   mul.hi.u32  %r5, %r1, %r2;
   USAD
   sad.u32     %r5, %r1, %r2, %r1;
*/


typedef enum {
  INTADD,
  INTSUB,
  INTMAD,
  INTMUL,
  INTDIV,
  INTREM,
  INTMIN,
  INTMAX,
  INTMUL24,
  INTMULHI,
  INTSAD
} INT_OP;


/*
ADD                                                                             
  add.s32     %r5, %r1, %r2;
*/
__global__ void int_add (int *my_array, int a, uint b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"add.s32 %0, %1, %0;\n\t"
		"add.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"add.s32 %0, %1, %0;\n\t"
		"add.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"add.s32 %0, %1, %0;\n\t"
		"add.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"add.s32 %0, %1, %0;\n\t"
		"add.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
SUB                                                                             
  sub.s32     %r5, %r2, %r1;
*/
__global__ void int_sub (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"sub.s32 %0, %1, %0;\n\t"
		"sub.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"sub.s32 %0, %1, %0;\n\t"
		"sub.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"sub.s32 %0, %1, %0;\n\t"
		"sub.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"sub.s32 %0, %1, %0;\n\t"
		"sub.s32 %0, %2, %0;\n\t")
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
MAD                                                                             
  mul.lo.s32  %r5, %r2, %r1;                                                    
  add.s32     %r6, %r5, %r1;
*/
__global__ void int_mad (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
		"mul.lo.s32 %2, %1, %0;\n\t"
		"add.s32 %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
MUL                                                                             
  mul.lo.s32  %r5, %r2, %r1;
*/
__global__ void int_mul (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"mul.lo.s32 %0, %1, %0;\n\t"
		"mul.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"mul.lo.s32 %0, %1, %0;\n\t"
		"mul.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"mul.lo.s32 %0, %1, %0;\n\t"
		"mul.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"mul.lo.s32 %0, %1, %0;\n\t"
		"mul.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
DIV                                                                             
  div.s32     %r5, %r1, %r2; 
*/
__global__ void int_div (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"div.s32 %0, %1, %0;\n\t"
		"div.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"div.s32 %0, %1, %0;\n\t"
		"div.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"div.s32 %0, %1, %0;\n\t"
		"div.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"div.s32 %0, %1, %0;\n\t"
		"div.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
REM                                                                             
  rem.s32     %r5, %r1, %r2; 
*/
__global__ void int_rem (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"rem.s32 %0, %1, %0;\n\t"
		"rem.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"rem.s32 %0, %1, %0;\n\t"
		"rem.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"rem.s32 %0, %1, %0;\n\t"
		"rem.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"rem.s32 %0, %1, %0;\n\t"
		"rem.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
MIN                                                                             
  min.s32     %r5, %r1, %r2;
*/
__global__ void int_min (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"min.s32 %0, %1, %0;\n\t"
		"min.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"min.s32 %0, %1, %0;\n\t"
		"min.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"min.s32 %0, %1, %0;\n\t"
		"min.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"min.s32 %0, %1, %0;\n\t"
		"min.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
MAX                                                                             
  max.s32     %r5, %r1, %r2; 
*/
__global__ void int_max (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"max.s32 %0, %1, %0;\n\t"
		"max.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"max.s32 %0, %1, %0;\n\t"
		"max.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"max.s32 %0, %1, %0;\n\t"
		"max.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"max.s32 %0, %1, %0;\n\t"
		"max.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
MUL24
  mul24.lo.s32    %r5, %r1, %r2;
*/
__global__ void int_mul24 (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"mul24.lo.s32 %0, %1, %0;\n\t"
		"mul24.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"mul24.lo.s32 %0, %1, %0;\n\t"
		"mul24.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"mul24.lo.s32 %0, %1, %0;\n\t"
		"mul24.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"mul24.lo.s32 %0, %1, %0;\n\t"
		"mul24.lo.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
MULHI                                                                          
  mul.hi.s32  %r5, %r1, %r2;    
*/
__global__ void int_mulhi (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"mul.hi.s32 %0, %1, %0;\n\t"
		"mul.hi.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"mul.hi.s32 %0, %1, %0;\n\t"
		"mul.hi.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"mul.hi.s32 %0, %1, %0;\n\t"
		"mul.hi.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"mul.hi.s32 %0, %1, %0;\n\t"
		"mul.hi.s32 %0, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}

/*
SAD                                                                            
  sad.s32     %r5, %r1, %r2, %r1;   
*/
__global__ void int_sad (int *my_array, int a, int b,  uint *start_t, uint *end_t)
{
  unsigned int start_time1;
  unsigned int start_time2;
  unsigned int start_time3;

  unsigned int end_time1;
  unsigned int end_time2;
  unsigned int end_time3;

  int i = a;
  int j = b;
  int k;

  asm volatile (
	  repeat128(
		"sad.s32 %0, %1, %2, %0;\n\t"
		"sad.s32 %0, %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );

  __syncthreads();

  start_time1 = clock();
  asm volatile (
	  repeat32(
		"sad.s32 %0, %1, %2, %0;\n\t"
		"sad.s32 %0, %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time1 = clock();

  __syncthreads();

  start_time2 = clock();
  asm volatile (
	  repeat64(
		"sad.s32 %0, %1, %2, %0;\n\t"
		"sad.s32 %0, %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time2 = clock();

  __syncthreads();

  start_time3 = clock();
  asm volatile (
	  repeat128(
		"sad.s32 %0, %1, %2, %0;\n\t"
		"sad.s32 %0, %1, %2, %0;\n\t"
	  )
	  : "=r"(k) : "r"(i) , "r"(j)
	  );
  end_time3 = clock();

  start_t[0] = start_time1;
  start_t[1] = start_time2;
  start_t[2] = start_time3;

  end_t[0] = end_time1;
  end_t[1] = end_time2;
  end_t[2] = end_time3;

  my_array[0] = i; 
  my_array[1] = j; 
  my_array[2] = k; 
}
