#include <algorithm>
#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/conv_layer.hpp"

#ifdef USE_CUDNN
#include "caffe/layers/cudnn_conv_layer.hpp"
#endif

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {


#ifdef USE_CUDNN

template <typename Dtype>
class CuDNNConvolutionLayerTest1_bwd : public GPUDeviceTest<Dtype> {
 protected:
  CuDNNConvolutionLayerTest1_bwd()
      : blob_bottom_(new Blob<Dtype>(64, 1, 28, 28)),
        blob_top_(new Blob<Dtype>(64, 20, 24, 24))
  {
    // fill the values
    FillerParameter filler_param;
    filler_param.set_value(1.);
    GaussianFiller<Dtype> filler(filler_param);
    filler.Fill(this->blob_bottom_);

    blob_bottom_vec_.push_back(blob_bottom_);
    blob_top_vec_.push_back(blob_top_);
  }

  virtual ~CuDNNConvolutionLayerTest1_bwd() {
    delete blob_bottom_;
    delete blob_top_;
  }


  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_top_;

  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

TYPED_TEST_CASE(CuDNNConvolutionLayerTest1_bwd, TestDtypes);

TYPED_TEST(CuDNNConvolutionLayerTest1_bwd, TestSimpleConvolutionCuDNN) {

  LayerParameter layer_param;
  ConvolutionParameter* convolution_param = layer_param.mutable_convolution_param();

  convolution_param->add_kernel_size(5);
  convolution_param->add_stride(1);
  convolution_param->set_num_output(50);
  convolution_param->mutable_weight_filler()->set_type("xavier");
  convolution_param->mutable_bias_filler()->set_type("constant");
  convolution_param->mutable_bias_filler()->set_value(0.1);

  shared_ptr<Layer<TypeParam> > layer(
	  new CuDNNConvolutionLayer<TypeParam>(layer_param));

  layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);

  // backward
  vector<bool> propagate_down(1, true); 

  layer->Backward(this->blob_top_vec_, propagate_down, this->blob_bottom_vec_);

}

#endif

}  // namespace caffe
