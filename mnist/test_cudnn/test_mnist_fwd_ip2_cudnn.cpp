#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/inner_product_layer.hpp"

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {

#ifndef CPU_ONLY
extern cudaDeviceProp CAFFE_TEST_CUDA_PROP;
#endif

template <typename TypeParam>
class MnistLenetFwdIp2Test_cudnn : public MultiDeviceTest<TypeParam> {
  typedef typename TypeParam::Dtype Dtype;
 protected:
  MnistLenetFwdIp2Test_cudnn()
      : blob_bottom_(new Blob<Dtype>(64,500,1,1)),
        blob_bottom_nobatch_(new Blob<Dtype>()),
        blob_top_(new Blob<Dtype>(64,10,1,1)) {

    // fill the values
    FillerParameter filler_param;
    UniformFiller<Dtype> filler(filler_param);
    filler.Fill(this->blob_bottom_);

    blob_top_vec_.push_back(blob_top_);
  }

  virtual ~MnistLenetFwdIp2Test_cudnn() {
    if(blob_bottom_) delete blob_bottom_;
    if(blob_bottom_nobatch_) delete blob_bottom_nobatch_;
    if(blob_top_) delete blob_top_;
  }

  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_bottom_nobatch_;
  Blob<Dtype>* const blob_top_;

  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

//TYPED_TEST_CASE(MnistLenetFwdIp2Test_cudnn, TestDtypesAndDevices);
TYPED_TEST_CASE(MnistLenetFwdIp2Test_cudnn, TestDtypesAndGpuDevices);


TYPED_TEST(MnistLenetFwdIp2Test_cudnn, TestForward) {
  typedef typename TypeParam::Dtype Dtype;

  this->blob_bottom_vec_.push_back(this->blob_bottom_);

  bool IS_VALID_CUDA = false;
#ifndef CPU_ONLY
  IS_VALID_CUDA = CAFFE_TEST_CUDA_PROP.major >= 2;
#endif

  if (Caffe::mode() == Caffe::CPU ||
      sizeof(Dtype) == 4 || IS_VALID_CUDA) {

    LayerParameter layer_param;
    InnerProductParameter* inner_product_param =
        layer_param.mutable_inner_product_param();

    inner_product_param->set_num_output(10);
    inner_product_param->mutable_weight_filler()->set_type("xavier");
    inner_product_param->mutable_bias_filler()->set_type("uniform");
    inner_product_param->mutable_bias_filler()->set_min(1);
    inner_product_param->mutable_bias_filler()->set_max(2);
	inner_product_param->set_transpose(false);

    shared_ptr<InnerProductLayer<Dtype> > layer(
        new InnerProductLayer<Dtype>(layer_param));

    layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
    layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);

  } else {
    LOG(ERROR) << "Skipping test due to old architecture.";
  }
}

}  // namespace caffe
