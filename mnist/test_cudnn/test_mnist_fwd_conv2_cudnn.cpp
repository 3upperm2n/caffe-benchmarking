#include <algorithm>
#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/conv_layer.hpp"

#ifdef USE_CUDNN
#include "caffe/layers/cudnn_conv_layer.hpp"
#endif

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {

  
#ifdef USE_CUDNN
template <typename Dtype>
class CuDNNConvolutionLayerTest2_mnist: public GPUDeviceTest<Dtype> {
 protected:
  CuDNNConvolutionLayerTest2_mnist()
      : blob_bottom_(new Blob<Dtype>(64, 20, 12, 12)),
        blob_bottom_2_(new Blob<Dtype>()),
        blob_top_(new Blob<Dtype>(64, 50, 8, 8)),
        blob_top_2_(new Blob<Dtype>()) {

    // fill the values
    FillerParameter filler_param;
    filler_param.set_value(1.);
    GaussianFiller<Dtype> filler(filler_param);
    filler.Fill(this->blob_bottom_);
    //filler.Fill(this->blob_bottom_2_);
    blob_bottom_vec_.push_back(blob_bottom_);
    blob_top_vec_.push_back(blob_top_);
  }

  virtual ~CuDNNConvolutionLayerTest2_mnist() {
    delete blob_bottom_;
    delete blob_bottom_2_;
    delete blob_top_;
    delete blob_top_2_;
  }

  virtual Blob<Dtype>* MakeReferenceTop(Blob<Dtype>* top) {
    this->ref_blob_top_.reset(new Blob<Dtype>());
    this->ref_blob_top_->ReshapeLike(*top);
    return this->ref_blob_top_.get();
  }

  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_bottom_2_;
  Blob<Dtype>* const blob_top_;
  Blob<Dtype>* const blob_top_2_;

  shared_ptr<Blob<Dtype> > ref_blob_top_;

  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

//TYPED_TEST_CASE(CuDNNConvolutionLayerTest2_mnist, TestDtypes);
TYPED_TEST_CASE(CuDNNConvolutionLayerTest2_mnist, TestDtypes);

TYPED_TEST(CuDNNConvolutionLayerTest2_mnist, TestSimpleConvolutionCuDNN) {

  LayerParameter layer_param;
  ConvolutionParameter* convolution_param = layer_param.mutable_convolution_param();

  convolution_param->add_kernel_size(5);
  convolution_param->add_stride(1);
  convolution_param->set_num_output(50);
  convolution_param->mutable_weight_filler()->set_type("xavier");
  convolution_param->mutable_bias_filler()->set_type("constant");
  convolution_param->mutable_bias_filler()->set_value(0.1);

  shared_ptr<Layer<TypeParam> > layer(new CuDNNConvolutionLayer<TypeParam>(layer_param));

  layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
}


#endif

}  // namespace caffe
