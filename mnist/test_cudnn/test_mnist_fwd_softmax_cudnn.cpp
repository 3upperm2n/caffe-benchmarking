#include <cmath>
#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/softmax_layer.hpp"

#ifdef USE_CUDNN
#include "caffe/layers/cudnn_softmax_layer.hpp"
#endif

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {
/*
template <typename TypeParam>
class MnistLenetFwdSoftmaxGpuTest : public MultiDeviceTest<TypeParam> {
  typedef typename TypeParam::Dtype Dtype;
 protected:
  MnistLenetFwdSoftmaxGpuTest()
      : blob_bottom_(new Blob<Dtype>(64, 10, 1, 1)),
        blob_top_(new Blob<Dtype>(64,10,1,1)) {
    // fill the values
    FillerParameter filler_param;
    GaussianFiller<Dtype> filler(filler_param);
    filler.Fill(this->blob_bottom_);

    blob_bottom_vec_.push_back(blob_bottom_);
    blob_top_vec_.push_back(blob_top_);
  }

  virtual ~MnistLenetFwdSoftmaxGpuTest() { 
	delete blob_bottom_; 
	delete blob_top_; 
  }

  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_top_;

  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

//TYPED_TEST_CASE(MnistLenetFwdSoftmaxGpuTest, TestDtypesAndDevices);
TYPED_TEST_CASE(MnistLenetFwdSoftmaxGpuTest, TestDtypesAndGpuDevices);

TYPED_TEST(MnistLenetFwdSoftmaxGpuTest, TestForward) {
  typedef typename TypeParam::Dtype Dtype;
  LayerParameter layer_param;
  SoftmaxLayer<Dtype> layer(layer_param);
  layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);
}
*/


#ifdef USE_CUDNN
template <typename Dtype>
class CuDNNMnistFwdSoftmaxTest : public GPUDeviceTest<Dtype> {
 protected:
  CuDNNMnistFwdSoftmaxTest()
	: blob_bottom_(new Blob<Dtype>(64, 10, 1, 1)),
	blob_top_(new Blob<Dtype>(64,10,1,1)) {

    // fill the values
    FillerParameter filler_param;
    GaussianFiller<Dtype> filler(filler_param);
    filler.Fill(this->blob_bottom_);

    blob_bottom_vec_.push_back(blob_bottom_);
    blob_top_vec_.push_back(blob_top_);
  }

  virtual ~CuDNNMnistFwdSoftmaxTest() { delete blob_bottom_; delete blob_top_; }

  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_top_;
  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

TYPED_TEST_CASE(CuDNNMnistFwdSoftmaxTest, TestDtypes);

TYPED_TEST(CuDNNMnistFwdSoftmaxTest, TestForwardCuDNN) {
  LayerParameter layer_param;
  CuDNNSoftmaxLayer<TypeParam> layer(layer_param);
  layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);
}

#endif
}  // namespace caffe
