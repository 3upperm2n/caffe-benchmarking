#include <algorithm>
#include <vector>

#include "google/protobuf/text_format.h"
#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"

#include "caffe/layers/absval_layer.hpp"
#include "caffe/layers/bnll_layer.hpp"
#include "caffe/layers/dropout_layer.hpp"
#include "caffe/layers/elu_layer.hpp"
#include "caffe/layers/exp_layer.hpp"
#include "caffe/layers/inner_product_layer.hpp"
#include "caffe/layers/log_layer.hpp"
#include "caffe/layers/power_layer.hpp"
#include "caffe/layers/prelu_layer.hpp"
#include "caffe/layers/relu_layer.hpp"
#include "caffe/layers/sigmoid_layer.hpp"
#include "caffe/layers/tanh_layer.hpp"
#include "caffe/layers/threshold_layer.hpp"

#ifdef USE_CUDNN
#include "caffe/layers/cudnn_relu_layer.hpp"
#include "caffe/layers/cudnn_sigmoid_layer.hpp"
#include "caffe/layers/cudnn_tanh_layer.hpp"
#endif

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {
  /*
	 template <typename TypeParam>
	 class MnistLenetFwdRelu1GpuTest : public MultiDeviceTest<TypeParam> {
	 typedef typename TypeParam::Dtype Dtype;

	 protected:
	 MnistLenetFwdRelu1GpuTest()
	 : blob_bottom_(new Blob<Dtype>(64, 500, 1, 1)),
	 blob_top_(new Blob<Dtype>()) {
	 Caffe::set_random_seed(1701);
  // fill the values
  FillerParameter filler_param;
  GaussianFiller<Dtype> filler(filler_param);
  filler.Fill(this->blob_bottom_);

  blob_bottom_vec_.push_back(blob_bottom_);
  blob_top_vec_.push_back(blob_top_);
  }

  virtual ~MnistLenetFwdRelu1GpuTest() { 
  delete blob_bottom_; 
  delete blob_top_; 
  }

  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_top_;
  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;

  };

  //TYPED_TEST_CASE(MnistLenetFwdRelu1GpuTest, TestDtypesAndDevices);
  TYPED_TEST_CASE(MnistLenetFwdRelu1GpuTest, TestDtypesAndGpuDevices);

  TYPED_TEST(MnistLenetFwdRelu1GpuTest, TestReLU) {
  typedef typename TypeParam::Dtype Dtype;
  LayerParameter layer_param;
  ReLULayer<Dtype> layer(layer_param);

  layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);
  }
  */

#ifdef USE_CUDNN
  template <typename Dtype>
	class CuDNNNeuronLayerTest1_mnist_fwd: public GPUDeviceTest<Dtype> {
	  protected:
		CuDNNNeuronLayerTest1_mnist_fwd()
		  : blob_bottom_(new Blob<Dtype>(64, 500, 1, 1)),
		  blob_top_(new Blob<Dtype>()) {
			Caffe::set_random_seed(1701);
			// fill the values
			FillerParameter filler_param;
			GaussianFiller<Dtype> filler(filler_param);
			filler.Fill(this->blob_bottom_);
			blob_bottom_vec_.push_back(blob_bottom_);
			blob_top_vec_.push_back(blob_top_);
		  }
		virtual ~CuDNNNeuronLayerTest1_mnist_fwd() { delete blob_bottom_; delete blob_top_; }
		Blob<Dtype>* const blob_bottom_;
		Blob<Dtype>* const blob_top_;
		vector<Blob<Dtype>*> blob_bottom_vec_;
		vector<Blob<Dtype>*> blob_top_vec_;
	};

  TYPED_TEST_CASE(CuDNNNeuronLayerTest1_mnist_fwd, TestDtypes);

  TYPED_TEST(CuDNNNeuronLayerTest1_mnist_fwd, TestReLUCuDNN) {
	LayerParameter layer_param;
	CuDNNReLULayer<TypeParam> layer(layer_param);
	layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
	layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);

  }

#endif

}  // namespace caffe
