#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/pooling_layer.hpp"

#ifdef USE_CUDNN
#include "caffe/layers/cudnn_pooling_layer.hpp"
#endif

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {

template <typename TypeParam>
class MnistLenetFwdPool1GpuTest : public MultiDeviceTest<TypeParam> {
  typedef typename TypeParam::Dtype Dtype;

 protected:
  MnistLenetFwdPool1GpuTest()
      : blob_bottom_(new Blob<Dtype>()),
        blob_top_(new Blob<Dtype>()),
        blob_top_mask_(new Blob<Dtype>()) {}

  /*
  virtual void SetUp() {
    Caffe::set_random_seed(1701);
    blob_bottom_->Reshape(2, 3, 6, 5);
    // fill the values
    FillerParameter filler_param;
    GaussianFiller<Dtype> filler(filler_param);
    filler.Fill(this->blob_bottom_);
    blob_bottom_vec_.push_back(blob_bottom_);
    blob_top_vec_.push_back(blob_top_);
  }
  */

  virtual ~MnistLenetFwdPool1GpuTest() {
    if(blob_bottom_) delete blob_bottom_;
    if(blob_top_) delete blob_top_;
    if(blob_top_mask_) delete blob_top_mask_;
  }

  Blob<Dtype>* blob_bottom_;
  Blob<Dtype>* blob_top_;
  Blob<Dtype>* blob_top_mask_;

  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

//TYPED_TEST_CASE(MnistLenetFwdPool1GpuTest, TestDtypesAndDevices);
TYPED_TEST_CASE(MnistLenetFwdPool1GpuTest, TestDtypesAndGpuDevices);



TYPED_TEST(MnistLenetFwdPool1GpuTest, TestSetupGlobalPooling) {
  typedef typename TypeParam::Dtype Dtype;

  LayerParameter layer_param;
  PoolingParameter* pooling_param = layer_param.mutable_pooling_param();

  pooling_param->set_kernel_size(2);
  pooling_param->set_stride(2);
  pooling_param->set_pool(PoolingParameter_PoolMethod_MAX);

  PoolingLayer<Dtype> layer(layer_param);

  this->blob_bottom_ = new Blob<Dtype>(64, 20, 24, 24);
  this->blob_top_ = new Blob<Dtype>(64, 20, 12, 12);

  // fill data                                                                  
  FillerParameter filler_param;                                                 
  UniformFiller<Dtype> filler(filler_param);                                    
  filler.Fill(this->blob_bottom_);                                              
  filler.Fill(this->blob_top_);                                                 
	


  this->blob_bottom_vec_.push_back(this->blob_bottom_);
  this->blob_top_vec_.push_back(this->blob_top_);

  layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);
}


}  // namespace caffe
