#!/bin/bash

nvprof --print-gpu-trace --csv ./matrixMul -device=0  2> gtx950_matrixMul_bs32_gpuTrace.csv
nvprof --metrics all --csv     ./matrixMul -device=0  2> gtx950_matrixMul_bs32_metrics.csv
