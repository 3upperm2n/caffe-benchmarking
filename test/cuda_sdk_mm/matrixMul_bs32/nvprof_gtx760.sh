#!/bin/bash

nvprof --print-gpu-trace --csv ./matrixMul -device=1  2> gtx760_matrixMul_bs32_gpuTrace.csv
nvprof --metrics all     --csv ./matrixMul -device=1  2> gtx760_matrixMul_bs32_metrics.csv
