#!/bin/bash

nvprof --print-gpu-trace --csv ./vectorAdd 1  2> gtx760_vectorAdd_bs32_gpuTrace.csv
nvprof --metrics all     --csv ./vectorAdd 1  2> gtx760_vectorAdd_bs32_metrics.csv
