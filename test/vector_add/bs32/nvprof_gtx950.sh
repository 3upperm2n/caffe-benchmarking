#!/bin/bash

nvprof --print-gpu-trace --csv ./vectorAdd 0  2> gtx950_vectorAdd_bs32_gpuTrace.csv
nvprof --metrics all     --csv ./vectorAdd 0  2> gtx950_vectorAdd_bs32_metrics.csv
