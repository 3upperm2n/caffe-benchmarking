#!/bin/bash

nvprof --print-gpu-trace --csv ./vectorAdd 1  2> gtx760_vectorAdd_bs64_gpuTrace.csv
nvprof --metrics all     --csv ./vectorAdd 1  2> gtx760_vectorAdd_bs64_metrics.csv
