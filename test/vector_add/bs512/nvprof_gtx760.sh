#!/bin/bash

nvprof --print-gpu-trace --csv ./vectorAdd 1  2> gtx760_vectorAdd_bs512_gpuTrace.csv
nvprof --metrics all     --csv ./vectorAdd 1  2> gtx760_vectorAdd_bs512_metrics.csv
