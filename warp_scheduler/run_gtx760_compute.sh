#!/bin/bash

./sm30_compute 1 16   > gtx760_persistent_all_b16.csv   2>&1
./sm30_compute 1 32   > gtx760_persistent_all_b32.csv   2>&1
./sm30_compute 1 64   > gtx760_persistent_all_b64.csv   2>&1
./sm30_compute 1 128  > gtx760_persistent_all_b128.csv  2>&1
./sm30_compute 1 256  > gtx760_persistent_all_b256.csv  2>&1
./sm30_compute 1 512  > gtx760_persistent_all_b512.csv  2>&1
./sm30_compute 1 1024 > gtx760_persistent_all_b1024.csv 2>&1

mv *.csv ./gtx760/compute/
