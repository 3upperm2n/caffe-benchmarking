#!/bin/bash

./sm52_mem 0 16   > gtx950_persistent_all_b16.csv   2>&1
./sm52_mem 0 32   > gtx950_persistent_all_b32.csv   2>&1
./sm52_mem 0 64   > gtx950_persistent_all_b64.csv   2>&1
./sm52_mem 0 128  > gtx950_persistent_all_b128.csv  2>&1
./sm52_mem 0 256  > gtx950_persistent_all_b256.csv  2>&1
./sm52_mem 0 512  > gtx950_persistent_all_b512.csv  2>&1
./sm52_mem 0 1024 > gtx950_persistent_all_b1024.csv 2>&1

mv *.csv ./gtx950/memory/
