#!/usr/bin/env python
""" Plotter """


import argparse
import random
import re
import pandas as pd
import numpy as np
from bokeh.plotting import figure, show, output_file
from bokeh.io import gridplot


def atoi(text):
    """ atoi function """
    return int(text) if text.isdigit() else text


def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [atoi(c) for c in re.split('(\d+)', text)]


def get_random_color():
    """ Get random color """
    return "#%06x" % random.randint(0, 0xFFFFFF)


class TimelinePlot(object):
    """TimelinePlot"""

    def __init__(self, csv_file):
        self.__csv_file = csv_file
        self.__dataframe = pd.read_csv(csv_file, skiprows=5)

    def get_plot(self, sm_filter=0):
        """Get timeline plot"""

        # Filter SM
        sm_trace = self.__dataframe.loc[self.__dataframe['sm'] == sm_filter]

        # find unique pair <block, warp > and their timeline
        unique_sm_trace = sm_trace.ix[:, 2:]

        # unqiue rows/records for the previous trace data
        unique_sm_trace = unique_sm_trace.drop_duplicates()

        # add y value for plotting purpose
        unique_sm_trace['y_axis'] = pd.Series(
            np.arange(1, len(unique_sm_trace.index) + 1),
            index=unique_sm_trace.index)

        plot = figure(width=450,
                      height=450,
                      title=self.__csv_file)
        plot.segment(x0=unique_sm_trace['start'],
                     y0=unique_sm_trace['y_axis'],
                     x1=unique_sm_trace['end'],
                     y1=unique_sm_trace['y_axis'],
                     color="#F4A582",
                     line_width=3)

        return plot


class GridPlot(object):
    """GridPlot"""

    def __init__(self, output_file_name, dim_x, csv_files):
        self.__file_name = output_file_name
        self.__dim_x = dim_x
        self.__plot_list = []
        for csv in csv_files:
            self.__plot_list.append(TimelinePlot(csv).get_plot())

    def plot(self):
        """Plot grid"""
        output_file(self.__file_name + '.html')

        figure_grid = []

        figure_row = []
        for index, value in enumerate(self.__plot_list):
            figure_row.append(value)
            if (index + 1) % self.__dim_x == 0:
                figure_grid.append(figure_row)
                figure_row = []
            if (index + 1) == len(self.__plot_list):
                figure_grid.append(figure_row)

        plots = gridplot(figure_grid)
        show(plots)


def main():
    # Arg parser
    parser = argparse.ArgumentParser(
        description='Timeline plotter')
    parser.add_argument('output', nargs=1, type=str,
                        help='Output file name')
    parser.add_argument('dimX', nargs=1, type=int,
                        help='Plot grid X')
    parser.add_argument('csv_file', nargs='+',
                        help='CSV files contain time data')
    args = parser.parse_args()

    args.csv_file.sort(key=natural_keys)
    if args.csv_file[0] != '*.csv':
        grid_plot = GridPlot(args.output[0],
                             args.dimX[0],
                             args.csv_file)
        grid_plot.plot()


if __name__ == '__main__':
    main()
