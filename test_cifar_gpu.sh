#!/bin/bash

if [ ! -d test_cifar ];then
  echo "creating test_cifar folder"
  mkdir test_cifar
fi

cd test_cifar

if [ ! -d caffe-benchmarking ];then
  echo "fetching github code"
  git clone https://bitbucket.org/3upperm2n/caffe-benchmarking.git
fi

cp caffe-benchmarking/cifar/test_gpu/*.cpp ../src/caffe/test/

# gpu kernels
cd caffe-benchmarking/cifar/test_gpu/


echo "extracting gpu test objects"
gputest_files=`ls | cut -d "." -f1`

# to the source level
cd ../../../../

# make tests
echo "generating test objects"
make -j$(nproc) test

cd ./build/test

# define the number of gpu devices on your current platform
GPUNUM=2

echo "profiling in progress"
for ((dev=0; dev<$GPUNUM; dev++)) 
do
  echo "Running on device : " $dev

  for f in $gputest_files;
  do
	# add extension
	target_obj="$f".testbin
	echo "start profiling " $target_obj

	# output file format : device number + filename + metrics + csv
	nvprof --print-gpu-trace ./$target_obj $dev  2>  dev"$dev"_"$f"_gpuTrace.csv
	nvprof --print-api-summary ./$target_obj $dev  2>  dev"$dev"_"$f"_apiSummary.csv
	nvprof --metrics all ./$target_obj $dev  2>  dev"$dev"_"$f"_metrics.csv
  
	echo "end profiling " $target_obj
  done
done


echo "end of profiling"
echo "check the csv files. Exiting ..."
