#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/pooling_layer.hpp"

#ifdef USE_CUDNN
#include "caffe/layers/cudnn_pooling_layer.hpp"
#endif

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {

#ifdef USE_CUDNN
template <typename Dtype>
class CuDNNPoolingLayerTest3_cifar: public GPUDeviceTest<Dtype> {
 protected:
   CuDNNPoolingLayerTest3_cifar()
	 : blob_bottom_(new Blob<Dtype>(100, 64, 8, 8)),
	 blob_top_(new Blob<Dtype>(100, 64, 4, 4)),
	 blob_top_mask_(new Blob<Dtype>()) {
	   Caffe::set_random_seed(1701);

	   // fill the values
	   FillerParameter filler_param;
	   GaussianFiller<Dtype> filler(filler_param);
	   filler.Fill(this->blob_bottom_);
	   blob_bottom_vec_.push_back(blob_bottom_);
	   blob_top_vec_.push_back(blob_top_);
	 }


  virtual ~CuDNNPoolingLayerTest3_cifar() {
    delete blob_bottom_;
    delete blob_top_;
    delete blob_top_mask_;
  }

  Blob<Dtype>* const blob_bottom_;
  Blob<Dtype>* const blob_top_;
  Blob<Dtype>* const blob_top_mask_;
  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
  // Test for 2x 2 square pooling layer
  // Test for 3x 2 rectangular pooling layer with kernel_h > kernel_w
  // Test for rectangular pooling layer with kernel_w > kernel_h
};

TYPED_TEST_CASE(CuDNNPoolingLayerTest3_cifar, TestDtypes);


TYPED_TEST(CuDNNPoolingLayerTest3_cifar, TestForwardMaxPaddedCuDNN) {
  // typedef typename TypeParam::Dtype Dtype;

  LayerParameter layer_param;
  PoolingParameter* pooling_param = layer_param.mutable_pooling_param();

  pooling_param->set_kernel_size(3);                                            
  pooling_param->set_stride(2);
  pooling_param->set_pool(PoolingParameter_PoolMethod_AVE);

  //PoolingLayer<Dtype> layer(layer_param);
  CuDNNPoolingLayer<TypeParam> layer(layer_param);

  layer.SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer.Forward(this->blob_bottom_vec_, this->blob_top_vec_);

}



#endif

}  // namespace caffe
