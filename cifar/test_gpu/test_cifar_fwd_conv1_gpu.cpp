#include <algorithm>
#include <vector>

#include "gtest/gtest.h"

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/filler.hpp"
#include "caffe/layers/conv_layer.hpp"

#ifdef USE_CUDNN
#include "caffe/layers/cudnn_conv_layer.hpp"
#endif

#include "caffe/test/test_caffe_main.hpp"
#include "caffe/test/test_gradient_check_util.hpp"

namespace caffe {

template <typename TypeParam>
class CifarFwdConv1GpuTest : public MultiDeviceTest<TypeParam> {
  typedef typename TypeParam::Dtype Dtype;

 protected:
  CifarFwdConv1GpuTest()
	: blob_bottom_(new Blob<Dtype>()),
	blob_bottom_2_(new Blob<Dtype>()),
	blob_top_(new Blob<Dtype>()),
	blob_top_2_(new Blob<Dtype>()) { 
	  /*
    Caffe::set_random_seed(1701);
    FillerParameter filler_param;
    blob_bottom_vec_.push_back(blob_bottom_);
    blob_top_vec_.push_back(blob_top_);
	*/
	}
 

  virtual ~CifarFwdConv1GpuTest() {
	if(blob_bottom_) delete blob_bottom_;
	if(blob_bottom_2_) delete blob_bottom_2_;
	if(blob_top_) delete blob_top_;
	if(blob_top_2_) delete blob_top_2_;
  }		

  //virtual ~MnistLenetFwdConv1GpuTest() { delete blob_bottom_; delete blob_top_; }

  Blob<Dtype>* blob_bottom_;
  Blob<Dtype>* blob_bottom_2_;
  Blob<Dtype>* blob_top_;
  Blob<Dtype>* blob_top_2_;
  //shared_ptr<Blob<Dtype> > ref_blob_top_;

  vector<Blob<Dtype>*> blob_bottom_vec_;
  vector<Blob<Dtype>*> blob_top_vec_;
};

TYPED_TEST_CASE(CifarFwdConv1GpuTest, TestDtypesAndGpuDevices);

// conv1 for lenet train test prototxt
TYPED_TEST(CifarFwdConv1GpuTest, cifar_fwd){
  typedef typename TypeParam::Dtype Dtype;                                      

  this->blob_bottom_ = new Blob<Dtype>(100, 3, 32, 32);
  this->blob_top_ = new Blob<Dtype>(100, 32, 32, 32);

  // fill data
  FillerParameter filler_param;
  UniformFiller<Dtype> filler(filler_param);
  filler.Fill(this->blob_bottom_);
  filler.Fill(this->blob_top_);
  

  this->blob_bottom_vec_.push_back(this->blob_bottom_);
  this->blob_top_vec_.push_back(this->blob_top_);

  LayerParameter layer_param;
  ConvolutionParameter* convolution_param =
	layer_param.mutable_convolution_param();
  convolution_param->add_kernel_size(5);
  convolution_param->add_pad(2);
  convolution_param->add_stride(1);
  convolution_param->set_num_output(32);
  convolution_param->mutable_weight_filler()->set_type("gaussian");
  convolution_param->mutable_weight_filler()->set_std(0.0001);
  convolution_param->mutable_bias_filler()->set_type("constant");
  convolution_param->mutable_bias_filler()->set_value(0.1);
  shared_ptr<Layer<Dtype> > layer(
	  new ConvolutionLayer<Dtype>(layer_param));
  layer->SetUp(this->blob_bottom_vec_, this->blob_top_vec_);
  layer->Forward(this->blob_bottom_vec_, this->blob_top_vec_);
}

}  // namespace caffe
