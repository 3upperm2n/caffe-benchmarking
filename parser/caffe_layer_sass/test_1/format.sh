#!/bin/bash

## remove the leading space
#sed -i.bak 's/^[ \t]*//' test1
sed -i 's/^[ \t]*//' test1

## delete predicates
sed -i 's/@!P[0-9]*//g' test1
sed -i 's/@P[0-9]*//g' test1

## remove lines with control code
sed -i '/^\/\* /d' test1

## remove lines with NOP and EXIT
sed -i '/NOP\|EXIT/d' test1 

## delete { and } 
sed -i -e 's/{//g' test1 
sed -i -e 's/}//g' test1 

## awk to print SASS and reditect them to a file
awk -F" " '{print $2;}' test1 > test1.result
