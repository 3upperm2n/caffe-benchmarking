#!/bin/bash

## find all the *.o files
targets=`find . -iname "*.o"`

## run cuobjdump -sass on them
## save the sass trace 
for f in $targets;
do
	#echo $f
	#echo $f | sed -e "s/\.\///g"  -e "s/\.o//g"
	output=$(echo $f | sed -e "s/\.\///g"  -e "s/\.o//g")
	echo $output
	cuobjdump -sass $f > "$output".sass 2>&1 
done




