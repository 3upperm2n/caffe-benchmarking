all: plot

plot:
	find . \( ! -regex '.*/\..*' \) -type d -exec bash -c "cd '{}' && rm -f all_in_one.html && $(PWD)/tool/plot.py all_in_one 3 *.csv" \;

clean:
	find . \( ! -regex '.*/\..*' \) -type d -exec bash -c "cd '{}' && rm -f all_in_one.html" \;

